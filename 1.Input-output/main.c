#include<stdio.h>
#include<stdlib.h>
#include<math.h>

/*Программа, рассчитывающая синус и косинус для каждого угла от 0 до 360*/
int main(int argc, const char **argv)
{
    FILE *f;
    int grad;
    f = fopen("sincos.txt", "w");
    if (!f)
    {
        perror("sincos.txt");
        return 1;
    }
    for(grad = 0; grad < 360; grad++)
    {
        double rads, s, c;
        rads = (double)grad * M_PI / 180.0;
        s = sin(rads);
        c = cos(rads);
        fprintf(f, "%03d %f %f \n   ", grad, s, c);
    }
    fclose(f);
    return 0;
}