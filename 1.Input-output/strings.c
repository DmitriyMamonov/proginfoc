#include<stdio.h>

/*Функция, читающая 50 символов из одного потока в другой*/
void string50(FILE *f1, FILE *f2);
/*Функция для перевода числа в строковое представление*/
void int_to_str(int n);

int main(int argc, const char **argv)
{
    int n;
    scanf("%d", &n);
    int_to_str(n);
}

void string50(FILE *f1, FILE *f2 )
{
    char buf[51];
    while (fgets(buf, sizeof(buf), f1))
    {
        int i;
        int nlpos = -1;
        for (i = 0; i < sizeof(buf) && buf[i]; i++)
        {
            if(buf[i] == '\n') 
            {
                nlpos = i;
                break;
            }
            if (nlpos == -1)
            {
                int c;
                fputc('\n',f2 );
                while ((c = fgetc(f1)) != EOF)
                {
                    if (c == '\n')
                    {
                        break;
                    }
                    
                }   
            }
            else
            {
                buf[nlpos] = '\0';
                fprintf(f2, "[%s] \n", buf);
            }
            
        }
        
    }
    
}

void int_to_str(int n) 
{
    char buf[32];
    sprintf(buf, "%d", n);
    fprintf(stdout, "%s\n", buf);
}