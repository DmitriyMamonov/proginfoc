#define MAX_NAME_LEN 64
#define MAX_GROUP_LEN 8
#pragma once
#include<stdio.h>

/*Структура, описывающая студента*/
struct student
{
    char name[MAX_NAME_LEN];
    char sex;
    char group[MAX_GROUP_LEN];
    int age;
    int year_of_birth;
    float average;
};

/*Вывести описание студента*/
void print_student(struct student *student)
{
    fprintf(stdout, "Имя %s, пол %d, группа %s, возраст %d, год рождения %d, средний балл %f\n", 
    student->name, student->sex, student->group, student->age, student->year_of_birth, student->average);    
}