#ifndef IO_H_ADD
#define IO_H_ADD

/*Чтение всех пар из файла*/
extern int read_info(const char *filename);
/*Выводит в стандартный поток все пары ключ-значение*/
extern void print_info();
/*Создание новой пары и запись в файл*/
extern int create_pair(const char *filename, const char *key, const char *value);
/*Получение значения из хеша по ключу*/
extern char *get_value(const char *key);

#endif
