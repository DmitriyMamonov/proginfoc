#include"../headers/io.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(int argc, const char **argv)
{
    if(!strcmp(argv[1], "--create"))
    {
        if ((int)strlen(argv[2]) > 10 || (int)strlen(argv[3]) > 10)
        {
            fprintf(stdout, "Invalid arguments!\n");
            exit(1);
        }

        return create_pair(argv[4], argv[2], argv[3]);
    }
    else if(!strcmp(argv[1], "--get"))
    {
        int result = read_info(argv[3]);
        char *value = get_value(argv[2]);
        if(!value)
        {
            perror("No such key!");
            exit(2);
        }
        else
        {
            fprintf(stdout, "Value -> %s\n", value);
        }
        return result;
    }
    else if(!strcmp(argv[1], "--help"))
    {
        /*Для задания собственного файла помощи назовите ваш файл help*/
        FILE *f;
        f = fopen("help", "r");
        int c;
        while ((c = fgetc(f)) != EOF)
        {
            fputc(c, stdout);
        }
    }
    else
    {
        int res = read_info(argv[2]);
        if (res == 0)
        {
            print_info();
        }
        return res;
    }
    return 0;
}
