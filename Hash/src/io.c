#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include"../headers/io.h"
#include"../headers/hash.h"
#define FIRST_SIZE 25

/*Временные хранилища записей в ОЗУ*/
static struct pair pairs[FIRST_SIZE];

/*Общее количество записей*/
static int pairs_size = 0;

int read_info(const char *filename)
{
    FILE *f;
    f = fopen(filename, "r");
    if(!f)
    {
	    perror(filename);
	    return 1;
    }	
    
    int c;
    for(int i = 0; ; i++)
    {
        if (pairs_size == FIRST_SIZE)
        {
            perror("All memory is based!");
            return 2;
        }
        
    	char *k;    
    	struct pair *pair = (struct pair *)malloc(sizeof(struct pair));
        
    	k = (char *)pair;
    	while((c = fgetc(f)) != EOF)
    	{
    	    if(c == '\n') break;
    	    *k = c;
    	    k++;	
    	}
        if (c == EOF) break;
        
    	pairs[i] = *pair;
        pairs_size++;
        free(pair);
    }
#ifdef DEBUG
    print_info();
#endif
    fclose(f);
    return 0;
}

void print_info()
{
    for (int i = 0; i < pairs_size; i++)
    {
        printf("Key: %s -> Value: %s\n", pairs[i].key, pairs[i].value);   
    }
}

int create_pair(const char *filename, const char *key, const char *value)
{
    struct pair *pair = (struct pair *)malloc(sizeof(struct pair));
    strcpy(pair->key, key);
    strcpy(pair->value, value);   
    FILE *f;
    f = fopen(filename, "a");
    if(!f)
    {
    	perror(filename);
    	return 1;
    }
    char *c;
    c = (char *)pair;
    for(int i = 0; i < sizeof(struct pair);i++)
    {
       fputc(*c++, f);
    }
    fputc('\n', f);
    fclose(f);    
    free(pair);
    return 0;
}
char *get_value(const char *key)
{
    for (int i = 0; i < pairs_size; i++)
    {
        if (!strcmp(key, pairs[i].key))
        {
            return pairs[i].value;
        }
    }
    return NULL;
}


